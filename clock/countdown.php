<?php

$secPerMin = 60;
$secPerHour = 60 * $secPerMin;
$secPerDay = 24 * $secPerHour;


//Current Time
$now = time();

//End of Semester Time
$eos = mktime(12,0,0,12,15,2019);

//Number of seconds between now and then
$seconds = $eos - $now;


$Days = floor($seconds/$secPerDay);
$seconds = $seconds - ($Days * $secPerDay);

$Hours = floor($seconds/$secPerHour);
$seconds = $seconds - ($Hours * $secPerHour);

$Minutes = floor($seconds/$secPerMin);
$seconds = $seconds - ($Minutes * $secPerMin);







?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Spencer's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include'../includes/header.php'?></header>
<nav><?php include'../includes/nav.php'?></nav>
<main>
    <h3>End of Semester Countdown</h3>
    <p>Days:<?=$Days ?> Hours:<?=$Hours ?> Minutes:<?=$Minutes ?> Seconds:<?=$seconds ?></p>
</main>
<footer><?php include'../includes/footer.php'?></footer>
</body>



</html>

