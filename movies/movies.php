<?php
class Movie{
    public $id;
    public $movietitle;
    public $rating;
    public $director;

    private $length=2;

    function displayTitle(){
        echo "<td>".$this->movietitle."</td>";
    }
    function displayLength(){
        echo "<td>".$this->length."</td>";
    }
    function displayId(){
        echo "<td>".$this->id."</td>";
    }
    function displayRating(){
        echo "<td>".$this->rating."</td>";
    }
}

$myMovie = new Movie();
$myMovie->id = 1;
$myMovie->movietitle = "Pretty Woman";
$myMovie->rating = "PG-13";
$myMovie->director = "Unknown";



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Spencer's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include'../includes/header.php'?></header>
<nav><?php include'../includes/nav.php'?></nav>
<main>
    <h3>My Movie List</h3>

<?php
$myMovie->displayLength();
$myMovie->displayTitle();
$myMovie->displayId();
$myMovie->displayRating();

?>
</main>
<footer><?php include'../includes/footer.php'?></footer>
</body>



</html>
