<?php
session_start();

//Did they ask a question


$DiceNum = array();
$DiceNum [0] = '<img src="../images/dice_1.png" alt="one">';
$DiceNum [1] = '<img src="../images/dice_2.png" alt="two">';
$DiceNum [2] = '<img src="../images/dice_3.png" alt="three">';
$DiceNum [3] = '<img src="../images/dice_4.png" alt="four">';
$DiceNum [4] = '<img src="../images/dice_5.png" alt="five">';
$DiceNum [5] = '<img src="../images/dice_6.png" alt="six">';


    //User dice 1
    $y = mt_rand(0,5);
    $iRoll = $y;
    $pd1 = $DiceNum[$iRoll];

    //User dice 2
    $x =  mt_rand(0,5);
    $iRoll = $x;
    $pd2 = $DiceNum[$iRoll];
    //Your Score
    $yourScore = ($x+1)+($y+1);


    //Computer dice 1
    $a = mt_rand(0,5);
    $iRoll = $a;
    $cd1 = $DiceNum[$iRoll];

    //Computer dice 2
    $b =  mt_rand(0,5);
    $iRoll = $b;
    $cd2 = $DiceNum[$iRoll];

    //Computer dice 3
    $c =  mt_rand(0,5);
    $iRoll = $c;
    $cd3 = $DiceNum[$iRoll];

   //Computer Score
    $computerScore = ($a+1)+($b+1)+($c+1);

    if($computerScore>$yourScore)
    {
        $Result = "Computer Wins!";
    }elseif ($computerScore==$yourScore){
        $Result = "Draw...";
    }else{
        $Result = "You Win!";
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Spencer's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include'../includes/header.php'?></header>
<nav><?php include'../includes/nav.php'?></nav>
<main>
    <div id="ur">
        <h2>Your Score</h2>
        <p><?=$pd1?><?=$pd2?></p>
        <h2><?=$yourScore?></h2>
    </div>

    <div id="cp">
        <h2>Computer Score</h2>
        <p><?=$cd1?><?=$cd2?><?=$cd3?></p>
        <h2><?=$computerScore?></h2>
    </div>

    <div id="res">
        <h2>Result:<?=$Result?></h2>
    </div>


</main>
<footer><?php include'../includes/footer.php'?></footer>
</body>


</html>