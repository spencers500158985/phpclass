<?php
session_start();

if(!isset($_SESSION["UID"])){
    header("Location:index.php");
}
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Spencer's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include'../includes/header.php'?></header>
<nav><?php include'../includes/nav.php'?></nav>
<main>
    <h1>Admin Page</h1>
    <form method="get">
        <table border="1" width="80%">
            <tr height="60">
                <td colspan="2"><h3>Add New Member</h3></td>
            </tr>
            <tr height="40">
                <th>Full Name</th>
                <td><input id ="txtFName" name="txtFName" type="text" size="50"></td>
            </tr>

            <tr height="40">
                <th>Email</th>
                <td><input id ="txtEmail" name="txtEmail" type="text" size="50"></td>
            </tr>

            <tr height="40">
                <th>Password</th>
                <td><input id ="txtPassword" name="txtPassword" type="password" size="50"></td>
            </tr>

            <tr height="40">
                <th>Retype Password</th>
                <td><input id ="txtPassword2" name="txtPassword2" type="password" size="50"></td>
            </tr>

            <tr height="40">
                <th>Role</th>
                <td>
                    <select id="txtRole" name="txtRole">
                        <option value="Admin">Admin</option>
                        <option value="Operator">Operator</option>
                        <option value="Member">Member</option>
                    </select>
                </td>
            </tr>



            <tr height="60">
                <td colspan="2">
                    <input type="submit" value="Add New Member">
                </td>
            </tr>

        </table>

    </form>
    <br />
</main>
<footer><?php include'../includes/footer.php'?></footer>
</body>



</html>
